<?php

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\sms\Direction;

/**
 * Implements hook_theme().
 */
function sms_ui_theme() {
  $items['sms_result'] = [
    'variables' => ['messages' => NULL],
  ];

  $items['sms_history'] = [
    'variables' => [
      'history' => NULL,
      'user' => NULL,
    ],
  ];

  $items['sms_ui_compose_form'] = [
    'render element' => 'element',
  ];

  $items['sms_ui_statistic'] = [
    'variables' => [
      'name' => NULL,
      'value' => NULL,
      'title' => NULL,
      'icon' => NULL,
    ],
  ];

  $items['sms_ui_wrap_summary_style'] = [
    'render element' => 'element',
  ];

  $items['sms_ui_nav'] = [
    'variables' => [
      'tree' => NULL,
      'trail' => NULL,
    ],
  ];

  return $items;
}

/**
 * Implements hook_form_BASE_FORM_ID_alter().
 */
function sms_ui_form_sms_gateway_form_alter(array &$form, FormStateInterface $form_state, $form_id) {
  // Disable retention duration field on gateway form, since this setting is
  // managed by SMS History settings globally.
  $form['message_queue']['retention_duration_outgoing']['#disabled'] = true;
  $form['message_queue']['retention_duration_outgoing']['#description']
    .= t(' Retention duration has been overridden by <a href=":link">SMS UI History Settings</a>',
    [':link' => Url::fromRoute('sms_ui.admin_form', [], ['fragment' => 'edit-message-history'])->toString()]);
}

/**
 * Implements hook_ENTITY_TYPE_presave().
 */
function sms_ui_sms_gateway_presave(EntityInterface $entity) {
  // Set gateway retention permanent. SMS History will handle cron cleanup.
  $entity->setRetentionDuration(Direction::OUTGOING, -1);
}

/**
 * Implements hook_cron().
 */
function sms_ui_cron() {
  \Drupal::service('sms_ui.history_processor')->cleanUpHistory();
  \Drupal::service('sms_ui.history_processor')->removeOrphans();
}
